# Ayo Coba Refactoring

Untuk mencoba refactoring, diberikan pilihan kelas sebagai berikut untuk direfactor. Semuanya ada pada controller DTKS Laravel.

1. \app\Http\Controllers\DashboardNasionalController.php
2. \app\Http\Controllers\MonitoringKabController.php
3. \app\Http\Controllers\PartialPenggantianPengurusController.php
4. \app\Http\Controllers\PenggantianPengurusController.php

Beberapa hal yang harus diperhatikan :

1. **Gunakan Modul** [Coding Style](https://gitlab.com/penelitianits/coding-style/-/blob/main/Coding-Style.md) dan [How to Refactor ](https://gitlab.com/penelitianits/coding-style/-/blob/main/howToRefactor.md) untuk membantu pengerjaan.

2. **JANGAN TAKUT SALAH** dalam melakukan uji coba refactor ini. Ikuti aturan yang ada apabila memang sebuah blok kode butuh dimodifikasi atau bahkan dihapus.

3. Hasil kode yang sudah direfactor dikirimkan ke siregarjonathan.19051@mhs.its.ac.id


<br><br><br><br>

#### Lampiran

![DashboardNasional](./Images/DashboardNasional.jpg)*Problem Measure DashboardNasional*

<br><br>

![MonitoringKab](./Images/MonitoringKab.jpg)*Problem Measure MonitoringKab*


<br><br>

![PartialPenggantian](./Images/PartialPenggantian.jpg)*Problem Measure PartialPenggantian*


<br><br>

![Penggantian](./Images/Penggantian.jpg)*Problem Measure Penggantian*



