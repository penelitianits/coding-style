# How to do Refactoring ft. Sonarlint

Refactoring berfungsi untuk **meningkatkan kualitas** dan keberlanjutan perangkat lunak dengan melakukan perubahan kecil pada kode yang sudah ada **tanpa mengubah fungsionalitas** eksternalnya. Refactoring dilakukan dengan tujuan untuk memperbaiki desain, meningkatkan kebersihan kode, mengurangi duplikasi kode, meningkatkan fleksibilitas, dan membuat kode lebih mudah dipahami dan dipelihara.

### 1. Install Sonarlint Extension pada IDE

<br>

### 2. Analisis Smell Code pada tab Problem

Dapat dilihat juga menggunakan shortcut `ctrl+shift+m`

Beberapa identifikasi problem pada Sonarlint :

<br>

#### Cyclomatic Complexity

Cyclomatic Complexity pada Sonarlint dihitung dengan banyaknya conditionals yang ada. Namun, apabila ditemukan nested conditionals (if atau switch di dalam if), maka nilai Complexitynya akan dilipatgandakan sesuai berapa kali conditionals itu di nested.


1. The Cyclomatic Complexity of this function is greater than 20 authorized. 

2. The Cyclomatic Complexity of this class is greater than 200 authorized, split this class.

3. Refactor this function to reduce its Cognitive Complexity to the 15 allowed. 

4. This method has returns more than the 3 allowed.

Problem di atas dapat di solve dengan meminimalisir [nested if](https://gitlab.com/penelitianits/coding-style/-/tree/main#1-nested-if), mengextract method, membuat fungsi abstraksi, dan meminimalisir penggunaan return lebih dari 3 dalam 1 function.


#### Duplication

Duplication dapat terjadi pada blok fuction atau kelas yang punya behaviour atau method yang sama. 

1. Define a constant instead of duplicating this literal.

Sonarlint mengidentifikasi penduplikasian suatu string sebagai indikator adanya duplikasi pada kode. Untuk itu sering dilakukan extract class seperti pada [Implementasi DRY](https://gitlab.com/penelitianits/coding-style/-/tree/main#4-dont-repeat-yourself-dry.).


#### Large Method

Sebuah method atau function yang terlalu besar sebaiknya dipecah agar tidak menumpuk beban.

1. This function  has lines, which is greater than the 150 lines authorized. Split it into smaller functions.

Dapat dilakukan extract class ataupun memecah dan membagi kelas yang ada sesuai fungsi-fungsinya masing-masing.



#### Problem Lainnya

Untuk problem lainnya akan diberikan instruksi langsung oleh Sonarlint, baik itu diremove, diberi space, dll. Setiap problem dapat diklik kanan untuk melihat rule dan contoh quick fix versi Sonarlint.





