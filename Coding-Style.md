# Coding Style

Coding style merupakan sekumpulan konvensi dan pedoman yang mengatur format kode, struktur kode, dan penulisan kode dalam suatu bahasa pemrograman. Gaya penulisan ini mencakup berbagai aspek tampilan dan organisasi kode, termasuk indentasi, jarak, konvensi penamaan, gaya komentar, dan lain-lain.
<br><br>


# Guidelines



## A. **Naming**

### 1. Nama yang Berarti

Nama yang digunakan harus **jelas** dan **merangkum tugas** yang dilakukan serta bukan **nama yang umum**.

Salah :
```php
// Nama yang tidak dapat dieja atau tidak sesuai fungsinya
$brtBdnBd = 56;
public function get(){      // Fungsi get digunakan untuk keperluan lain menurut konvensi
    $age = 177;
}
```
<br>

Benar :
```php
$budiWeight = 67;
public function getDataHeight(){
    $height = 177;
}

```
<br>

### 2. Aturan penamaan sesuai Konvensi

- Nama kelas menggunakan format PascalCase

```php
class ValidationController extends BaseController
{
    ...
}
```

- Nama fungsi, field, dan parameter menggunakan format camelCase

```php
public function validationUsulanPbi($user, $idSemesta, $nik)
    {
```
}

- Nama konstanta menggunakan format UPPER_SNAKE_CASE

```php
define (NO_PROV_JAWA_TIMUR, 14);
$noProv = NO_PROV_JAWA_TIMUR;
```

<br>


## B. Visibilitas

public, private, protected, atau default?

Setiap entitas seharusnya terbuka untuk extension akan tetapi tertutup untuk dimodifikasi pihak luar.

| Modifier | Class | Package | Subclass | World |
| ------ | ------ | ------ | ------ | ------ |
| Public |    1    |     1   |    1    |    1   |
| Protected |   1     |   1     |   1     |   0    |
| Default |    1    |   1     |      0  |    0   |
| Private |    1    |    0    |     0  |    0   |

Tabel di atas menunjukkan aksesibilitas suatu entitas berdasarkan modifier.

<br><br>


## C. Function

### 1. Nested-If

Fungsi dengan nested-if (if di dalam if di dalam if) yang terlalu banyak menyulitkan pembacaan dan meningkatkan kompleksitas secara drastis. 

Untuk setiap return dalam if, kompleksitasnya dihitung 1 poin. Apabila if tersebut ada di dalam if, if di dalam if dihitung 2. Apabila ada if di dalam if di dalam if, kompleksitas if paling dalam dihitung 3, dan seterusnya. kompleksitas yang nilainya semakin tinggi berbanding lurus dengan beban dari fungsi atau kelas itu sendiri.

#### Hindari Terlalu Banyak Return

Salah :
```php
function isShopOpen($day): bool
{
    if ($day) {
        if (is_string($day)) {
            $day = strtolower($day);
            if ($day === 'friday') {
                return true;
            } elseif ($day === 'saturday') {
                return true;
            } elseif ($day === 'sunday') {
                return true;
            }
            return false;
        }
        return false;
    }
    return false;
}
```
<br>

Benar :
```php
function isShopOpen(string $day): bool
{
    if (empty($day)) {
        return false;
    }

    $openingDays = ['friday', 'saturday', 'sunday'];

    return in_array(strtolower($day), $openingDays, true);
}
```
<br><br>

#### Hindari Penggunaan Nested-If Berlebih

Salah :

```php
function fibonacci(int $n)
{
    if ($n < 50) {
        if ($n !== 0) {
            if ($n !== 1) {
                return fibonacci($n - 1) + fibonacci($n - 2);
            }
            return 1;
        }
        return 0;
    }
    return 'Not supported';
}
```

Benar :
```php
function fibonacci(int $n): int
{
    if ($n === 0 || $n === 1) {
        return $n;
    }

    if ($n >= 50) {
        throw new Exception('Not supported');
    }

    return fibonacci($n - 1) + fibonacci($n - 2);
}
```


### 2. Argument Function < 3

Argument yang ada pada function sebaiknya tidak terlalu banyak untuk meringankan kerja function tsb. Cukup 2 atau kurang dari 2.

Salah :
```php
class PBIValidation
{
    public function __construct(
        string $firstname,
        string $lastname,
        string $age,
        string $region,
        string $district,
        string $city,
        string $phone,
        string $email
    ) {
        // ...
    }
}
```
<br>

Benar :
```php
class Identity
{
    private $firstname;
    private $lastname;
    private $age;

    public function __construct(string $firstname, string $lastname, string $age)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->patronymic = $age;
    }

    // getters ...
}

class City
{
    private $region;
    private $district;
    private $city;

    public function __construct(string $region, string $district, string $city)
    {
        $this->region = $region;
        $this->district = $district;
        $this->city = $city;
    }

    // getters ...
}

class Contact
{
    private $phone;
    private $email;

    public function __construct(string $phone, string $email)
    {
        $this->phone = $phone;
        $this->email = $email;
    }

    // getters ...
}

class PBIValidation
{
    public function __construct(Identity $identity, City $city, Contact $contact)
    {
        // ...
    }
}

```

Terlihat kelas semakin banyak, begitu pula dengan lines of code. Membagi kelas atas tugas yang kecil dan tunggal merupakan implementasi dari Single Responsibility Principle (SRP).


### 3. Hindari Variable Negatif

Penggunaan Variable positif memudahkan programmer dalam me-reuse variable tsb.

Salah :
```php
if (isBansosNotReceived($idBansos)) {
    // ...
}
```

<br>
Benar :
```php
if (! isBansosReceived($idBansos)) {
    // ...
}
```


### 4. Don't Repeat Yourself (DRY)

Beberapa function dapat memiliki method atau behaviour yang mirip. Dapat ditambahkan function abstrak yang menyatukan beberapa function tersebut.

Salah :
```php
function validateFormA($input) {
  if (empty($input['name'])) {
    return 'Name is required.';
  }

  if (empty($input['email'])) {
    return 'Email is required.';
  }

  // Validasi lainnya untuk Form A...

  return 'Form A is valid.';
}

function validateFormB($input) {
  if (empty($input['name'])) {
    return 'Name is required.';
  }

  if (empty($input['phone'])) {
    return 'Phone number is required.';
  }

  // Validasi lainnya untuk Form B...

  return 'Form B is valid.';
}
```

Kedua function memiliki method yang sama untuk memvalidasi Name.

<br><br>
Benar :

```php
function validateFormA($input) {
  if (empty($input['name'])) {
    return 'Name is required.';
  }

  if (empty($input['email'])) {
    return 'Email is required.';
  }

  // Validasi lainnya untuk Form A...

  return 'Form A is valid.';
}

function validateFormB($input) {
  if (empty($input['name'])) {
    return 'Name is required.';
  }

  if (empty($input['phone'])) {
    return 'Phone number is required.';
  }

  // Validasi lainnya untuk Form B...

  return 'Form B is valid.';
}

```

## D. Comment

Sebuah kode seharusnya dapat menjelaskan dirinya sendiri. Comment digunakan untuk MEMPERJELAS kode yang dianggap butuh penjelasan, bukan menyimpan kode yang tidak digunakan.

Comment lebih baik tidak digunakan. Hanya boleh digunakan untuk :
- Memberi Informasi Legalitas
- Memberi Peringatan
- Memberi Penjelasan di luar kode

Contoh :

```php
/**
 * Company: Google Corporation
 * Author: Rimaz Santiago
 * Date: 2021-06-01
 *
 * This code is the property of Google Corporation. Unauthorized use or duplication
 * of this code is strictly prohibited.
 */


// DO NOT RUN THIS CLASS ON PERSONAL COMPUTER

 public class NuclearBomb{
     $time = 5;             // Count down time in minutes
     $bomb = "shut down server";  
 }

```

#### Hapus Kode Mati

Kode yang tidak digunakan lagi seringkali di'comment' dengan harapan untuk dapat digunakan kembali nantinya. Programmer punya version history untuk melakukan hal tsb.



<br><br>

## E. Unit Test

#### Test Penjumlahan 1

```php
// Arrange
$var = 5;

// Act
$var += 6;

// Assert
$this->assertEquals($var, 11);

```

#### Test Penjumlahan 2

```php

// Kelas Penjumlahan

Class Penjumlahan {

private $angkaPertama;
private $angkaKedua;

    public function tambah($angkaPertama, $angkaKedua){
        return $angkaPertama, $angkaKedua;
    }
}
```

Unit Test

```
public function testTambah()
{
    $penjumlahan = new Penjumlahan();

    // Test case 1
    $result = $penjumlahan->tambah(2, 3);
    $this->assertEquals(5, $result);

    // Test case 2
    $result = $penjumlahan->tambah(0, 0);
    $this->assertEquals(0, $result);

    // Test case 3
    $result = $penjumlahan->tambah(-5, 7);
    $this->assertEquals(2, $result);
}

```
<br><br>



# Related Tools


## Sonarqube

Sonarqube menawarkan proses scanning secara menyeluruh pada kode. Beberapa aspek penting yang dapat diketahui :

- Code **Quality**
    <br>&nbsp;&nbsp;&nbsp;&nbsp; 
    Mengidentifikasi Code Smell, Bug, Vulnerabillities, Technical Debt
- Code **Coverage** 
    <br>&nbsp;&nbsp;&nbsp;&nbsp; 
    Mengidentifikasi kode yang tidak memiliki unit test
- Code **Duplication** 
    <br>&nbsp;&nbsp;&nbsp;&nbsp; 
    Mengidentifikasi blok kode yang mirip
- Code **Documentation** 
    <br>&nbsp;&nbsp;&nbsp;&nbsp; 
    Comment dalam kode yang membantu keterbacaan
- Code **Maintainability** 
    <br>&nbsp;&nbsp;&nbsp;&nbsp; 
    Complexity, Coupling, Readability
- Coding **Standard**
    <br>&nbsp;&nbsp;&nbsp;&nbsp; 
    Tata cara penulisan sesuai konvensi umum



### How to Install

#### Sonarlint via VS Code

1. Pergi ke menu "Tambahkan Extension"
2. Cari Extension `sonarlint` kemudian install


#### Sonarqube

0. Requirement : JDK 11

1. Pergi ke  https://www.sonarsource.com/products/sonarqube/downloads/ lalu download Community Edition

2. Extract File yang didownload

3. Uncomment database yang digunakan pada `./[sonarqube-version]/conf/sonar.properties`

4. Buka `./[Sonarqube-version]/bin/` lalu buka folder sesuai Sistem Operasi PC

5. Start Sonarqube dengan run `./bin/[OperatingSystem]/sonar.sh start`

6. Apabila sudah berhasil di run, buka http://localhost:9000 pada browser

7. Login dengan <br>
```
Username: admin
Password: admin
```




















<br><br><br><br>








Referensi :
https://github.com/piotrplenik/clean-code-php
